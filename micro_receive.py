import micropython
# Disable CTRL-C Interrupt (0x03)
micropython.kbd_intr(-1)

from microbit import uart, display, sleep
import radio

micropython.mem_info()
####################
#   UART           #
####################

        
def uart_read():

    global UART_BUFFER

    # Check if there are waiting data in the buffer
    if (uart.any()):
        #micropython.mem_info()

        # Add UART data to Buffer (up to 500 bytes)
        UART_BUFFER.extend(uart.read(200))

        while (True):

            # Each packet start with 0xFF so we trim the begining of the buffer until a packet start
            uart_trim_buffer()

            # Check that the buffer contain at least the size
            bufferSize = len(UART_BUFFER)
            if (bufferSize <=1 ):
                break

            # Packet lenght is defined at the 2nd byte
            expectedSize = UART_BUFFER[1]
            # Check that the packet is complet in buffer
            if bufferSize < expectedSize:
                break
            
            # Extract the pack from buffer
            packet = UART_BUFFER[:expectedSize]
            UART_BUFFER = UART_BUFFER[expectedSize:]

            # Treat packet
            uart_handle_packet(packet)


def uart_trim_buffer():

    global UART_BUFFER

    # Each packet start with 0xFF so we trim the begining of the buffer until a packet start
    bufferSize = len(UART_BUFFER)

    i = 0
    while True:

        # No more byte in buffer to check
        if (i >= bufferSize-1):
            break

        # Found Starting Byte
        if (UART_BUFFER[i] == 0xff):

            # If Lenght byte defined and is 0 we continue trimming
            if (i+1 <= bufferSize-1 and UART_BUFFER[i+1] == 0):
                i+=1
                continue

            break

        i+=1

    UART_BUFFER = UART_BUFFER[i:]
    

def uart_handle_packet(upacket):
    if not uart_p_verify_checksum(upacket):
        print("dropping packet: "+str(upacket))
        return

    rfp = rf_p_create(b'0', REMOTE_ADDRESS, upacket)

    rf_p_to_send.append(rfp)

####################
#   RADIO PACKET   #
####################

def uart_p_get_header(uartp):
    return uartp[:UART_P_HEADER_SIZE]

def uart_p_get_payload(uartp):
    return uartp[UART_P_HEADER_SIZE:-UART_P_CHECKSUM_SIZE]

def uart_p_get_checksum(uartp):
    return uartp[-UART_P_CHECKSUM_SIZE:]

def uart_p_create_checksum(uartp):
    return (sum(bytearray()+uart_p_get_header(uartp)+uart_p_get_payload(uartp)) & (0xFF+0xFF)).to_bytes(2, 'big')

def uart_p_verify_checksum(uartp):
    return uart_p_get_checksum(uartp) == uart_p_create_checksum(uartp)

####################
#  RADIO           #
####################
        
def rf_send_packet(bytes):
    print('sending: '+str(bytes))
    radio.send_bytes(bytes)
        
def rf_check_rf():
    
    details = radio.receive_full()
    if (details):        
        bytes, rssi, timestamp = details
        #print("msg: "+str(msg))
        #print(bytes)
        #print("rssi: "+str(rssi))
        #print("timestamp: "+str(timestamp))
        rf_handle_packet(bytes)

def rf_handle_packet(rfp):
        dst = rf_p_get_dst(rfp)

        if not rf_p_verify_checksum(rfp):
            print('[RF] Checksum error')
            return

        if (dst != RF_ADDRESS.to_bytes(2, 'big')):
            print('this message is not for me')
            return
        
        src = rf_p_get_src(rfp)
        if (src != REMOTE_ADDRESS.to_bytes(2, 'big')):
            print('this message is not from valid source')
            return

        uart.write(rf_p_get_payload(rfp))



####################
#   RADIO PACKET   #
####################

def rf_p_get_dst(rfp):
    return rfp[8:10]

def rf_p_get_src(rfp):
    return rfp[6:8]
    
def rf_p_get_header(rfp):
    return rfp[:RF_P_HEADER_SIZE]

def rf_p_get_payload(rfp):
    return rfp[RF_P_HEADER_SIZE: int(rfp[0])-RF_P_CHECKSUM_SIZE]

def rf_p_get_checksum(rfp):
    return rfp[-RF_P_CHECKSUM_SIZE:]

def rf_p_verify_checksum(rfp):
    return rf_p_get_checksum(rfp) == rf_p_create_checksum(rfp)

def rf_p_create_checksum(rfp):
    return (sum(bytearray()+rf_p_get_header(rfp)+rf_p_get_payload(rfp)) & (0xFF+0xFF)).to_bytes(2, 'big')

def rf_p_create( type, dst, payload ):
    rfp = bytearray()

    lenght = RF_P_HEADER_SIZE + RF_P_CHECKSUM_SIZE + len(payload)

    # LENGHT 1 Byte
    rfp.extend(lenght.to_bytes(1, 'big'))
    # TYPE 1 Byte (0: data, 1: ack)
    rfp.extend(type)

    # SEQ 4 Bytes
    global RF_P_SEQ
    RF_P_SEQ += 1
    rfp.extend(RF_P_SEQ.to_bytes(4, 'big'))

    # SRC 2 Bytes
    rfp.extend(RF_ADDRESS.to_bytes(2, 'big'))
    # DST 2 Bytes
    rfp.extend(dst.to_bytes(2, 'big'))

    # PAYLOAD
    rfp.extend(payload)

    # CHECKSUM 2 bytes
    rfp.extend(rf_p_create_checksum(rfp))

    return rfp

####################
# UART CONFIG      #
####################
UART_P_HEADER_SIZE = 9
UART_P_CHECKSUM_SIZE = 2

UART_BUFFER = bytearray()

uart.init(115200)

####################
# RADIO CONFIG     #
####################
RF_P_HEADER_SIZE = 10
RF_P_CHECKSUM_SIZE = 2

RF_P_SEQ = 100

RF_ADDRESS = 2
REMOTE_ADDRESS = 1

radio.on()
radio.config(length=64+RF_P_HEADER_SIZE+RF_P_CHECKSUM_SIZE+5, channel=42, power=7, address=0x75626974, group=25, queue=10)

####################
# MAIN             #
####################

micropython.mem_info()
p =rf_p_create(b'0', REMOTE_ADDRESS, b'TEST')
rf_send_packet(p)

# Packets RF to send
rf_p_to_send = []

# Boucle Runtime
while True:

    uart_read()

    for _ in rf_p_to_send:
        rf_send_packet(rf_p_to_send.pop(0))

    rf_check_rf()

#   sleep(100)


