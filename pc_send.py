import serial
import time
import requests 
from Crypto.Cipher import AES
from base64 import b64decode, b64encode
import json

# Don't forget to establish the right serial port ******** ATTENTION
SERIALPORT0 = "/dev/ttyACM0"
BAUDRATE = 115200

def initUART(serialport):

    ser = serial.Serial()
    ser.port=serialport
    ser.baudrate=BAUDRATE
    ser.bytesize = serial.EIGHTBITS #number of bits per bytes
    ser.parity = serial.PARITY_NONE #set parity check: no parity
    ser.stopbits = serial.STOPBITS_ONE #number of stop bits
    ser.timeout = 0          #block read
    ser.xonxoff = False     #disable software flow control
    ser.rtscts = False     #disable hardware (RTS/CTS) flow control
    ser.dsrdtr = False       #disable hardware (DSR/DTR) flow control

    return ser

def openSerial(ser):

    print ("Starting Up Serial Monitor")
    try:
            ser.open()
    except serial.SerialException:
            print("Serial {} port not available".format(ser.port))
            exit()

def closeUART(ser):
    ser.close()

UART_MICROBIT_BUFFER = 63

UART_PACKET_HEADER_SIZE = 9
UART_PACKET_ENDING_SIZE = 2 # checksum
UART_PACKET_DATA_SIZE = UART_MICROBIT_BUFFER - UART_PACKET_HEADER_SIZE - UART_PACKET_ENDING_SIZE

# Starting sequence
UART_PACKET_SEQUENCE = 0


class UART_Packet:

    def __init__(self, lenght, type, seq, tnb, nb, payload, checksum):

        self.lenght = lenght
        self.type = type
        self.seq = seq
        self.tnb = tnb
        self.nb = nb

        self.payload = payload

        self.checksum = checksum

    def getHeader(self):
        return self.lenght+self.type+self.seq+self.tnb+self.nb

    def serialize(self):
        return b'\xFF'+self.getHeader()+self.payload+self.checksum


def create_UART_Packet(type, seq, tnb, nb, payload):

    lenght_int = len(payload) + UART_PACKET_HEADER_SIZE + UART_PACKET_ENDING_SIZE

    #print("taille"+str(lenght_int))
    lenght_bytes = lenght_int.to_bytes(1, 'big')

    checksum = (sum(bytearray()+b'\xFF'+lenght_bytes+type+seq+tnb+nb+payload) & (0xFF+0xFF)).to_bytes(2, 'big')
    #print("checksum: "+str(checksum))

    return UART_Packet(lenght_bytes, type, seq, tnb, nb, payload, checksum)

def createPacketsForMessage(message):

    payloadSize = len(message)
    nbPackets = payloadSize // UART_PACKET_DATA_SIZE + 1
    type = (0).to_bytes(1, 'big');

    global UART_PACKET_SEQUENCE
    UART_PACKET_SEQUENCE += 1
    seq = UART_PACKET_SEQUENCE.to_bytes(4, 'big')

    tnb = nbPackets.to_bytes(1, 'big')

    packets = []
    for i in range(0, nbPackets):
        payload = message[i*UART_PACKET_DATA_SIZE: (i+1)*UART_PACKET_DATA_SIZE]
        packets.append(create_UART_Packet(type, seq, tnb, (i+1).to_bytes(1, 'big'), bytes(payload, 'utf-8')))

    return packets

def sendUARTMessage(ser, msg):

    for p in createPacketsForMessage(msg):
        time.sleep(0.04)
        #print(p.serialize())
        ser.write(p.serialize())


SIM_URL = "http://localhost:3001/sensors"

def serialize_sensors_data(sensors):

    data = ''

    for sensor in sensors:
        data += str(sensor['id'])
        data += ':'
        data += str(sensor['value'])
        data += ','

    data = data[:-1]
    return data

def get_sensors_data():
    r = requests.get(SIM_URL).json()
    result = []

    # Chunk of n sensors
    n = 5
    for i in range(0, len(r), n):
        result.append(serialize_sensors_data(r[i:i + n]))

    return result

def cypher_data(data):
    print(data)

    key = bytearray("OscarLeBeauGosse", 'utf-8')
    cipher = AES.new(key, AES.MODE_CTR)
    ct_bytes = cipher.encrypt(bytes(data, 'utf-8'))
    nonce = b64encode(cipher.nonce).decode('utf-8')
    ct = b64encode(ct_bytes).decode('utf-8')
    result = nonce + ct
    return result

ser0 = initUART(SERIALPORT0)
openSerial(ser0)

try:
    while ser0.isOpen() : 

        dataArray = get_sensors_data()
        for data in dataArray:
            encryptedData = cypher_data(data)
            sendUARTMessage(ser0, encryptedData)
        time.sleep(0.8)




except (KeyboardInterrupt, SystemExit):
    closeUART(ser0)
    exit()