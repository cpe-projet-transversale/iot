# IOT
``` bash
    lpcprog -d /dev/ttyUSB0 -c id
```

``` bash
    lpcprog -d /dev/ttyUSB0 -c flash [file.bin]
```

``` bash
    minicom -D /dev/ttyUSB0
```

pip install pycryptodome