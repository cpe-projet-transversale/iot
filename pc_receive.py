import serial
import time
from Crypto.Cipher import AES
from base64 import b64decode, b64encode
import requests 

SERIALPORT0 = "/dev/ttyACM1"
BAUDRATE = 115200

def initUART(serialport):

    ser = serial.Serial()
    ser.port=serialport
    ser.baudrate=BAUDRATE
    ser.bytesize = serial.EIGHTBITS #number of bits per bytes
    ser.parity = serial.PARITY_NONE #set parity check: no parity
    ser.stopbits = serial.STOPBITS_ONE #number of stop bits
    ser.timeout = 0          #block read

    ser.xonxoff = False     #disable software flow control
    ser.rtscts = False     #disable hardware (RTS/CTS) flow control
    ser.dsrdtr = False       #disable hardware (DSR/DTR) flow control

    return ser

def openSerial(ser):

    print ("Starting Up Serial Monitor")
    try:
            ser.open()
    except serial.SerialException:
            print("Serial {} port not available".format(ser.port))
            exit()

def closeUART(ser):
    ser.close()
####################
#   UART           #
####################

        
def uart_read(ser):

    global UART_BUFFER

    # Check if there are waiting data in the buffer
    if (ser.inWaiting()):

        # Add UART data to Buffer (up to 500 bytes)
        UART_BUFFER.extend(ser.read(200))

        while (True):

            # Each packet start with 0xFF so we trim the begining of the buffer until a packet start
            uart_trim_buffer()

            # Check that the buffer contain at least the size
            bufferSize = len(UART_BUFFER)
            if (bufferSize <=1 ):
                break

            # Packet lenght is defined at the 2nd byte
            expectedSize = UART_BUFFER[1]
            # Check that the packet is complet in buffer
            if bufferSize < expectedSize:
                break
            
            # Extract the pack from buffer
            packet = UART_BUFFER[:expectedSize]
            UART_BUFFER = UART_BUFFER[expectedSize:]

            # Treat packet
            return uart_handle_packet(bytes(packet))


def uart_trim_buffer():

    global UART_BUFFER

    # Each packet start with 0xFF so we trim the begining of the buffer until a packet start
    bufferSize = len(UART_BUFFER)

    i = 0
    while True:

        # No more byte in buffer to check
        if (i >= bufferSize-1):
            break

        # Found Starting Byte
        if (UART_BUFFER[i] == 0xff):

            # If Lenght byte defined and is 0 we continue trimming
            if (i+1 <= bufferSize-1 and UART_BUFFER[i+1] == 0):
                i+=1
                continue

            break

        i+=1

    UART_BUFFER = UART_BUFFER[i:]

def uart_handle_packet(upacket):
    if not uart_p_verify_checksum(upacket):
        print("dropping packet: "+str(upacket))
        return
    
    key = bytearray("OscarLeBeauGosse", 'utf-8')

    try:
        payload = uart_p_get_payload(upacket)
        nonce = b64decode(payload[:12])
        ct = b64decode(payload[12:])
        cipher = AES.new(key, AES.MODE_CTR, nonce=nonce)
        pt = cipher.decrypt(ct)
        return(pt)
    except:
        return None

####################
#   RADIO PACKET   #
####################

def uart_p_get_header(uartp):
    return uartp[:UART_P_HEADER_SIZE]

def uart_p_get_payload(uartp):
    return uartp[UART_P_HEADER_SIZE:-UART_P_CHECKSUM_SIZE]

def uart_p_get_checksum(uartp):
    return uartp[-UART_P_CHECKSUM_SIZE:]

def uart_p_create_checksum(uartp):
    return (sum(bytearray()+uart_p_get_header(uartp)+uart_p_get_payload(uartp)) & (0xFF+0xFF)).to_bytes(2, 'big')

def uart_p_verify_checksum(uartp):
    return uart_p_get_checksum(uartp) == uart_p_create_checksum(uartp)

####################
# UART CONFIG      #
####################
UART_P_HEADER_SIZE = 9
UART_P_CHECKSUM_SIZE = 2

UART_BUFFER = bytearray()

UART_MICROBIT_BUFFER = 63
UART_PACKET_DATA_SIZE = UART_MICROBIT_BUFFER - UART_P_HEADER_SIZE - UART_P_CHECKSUM_SIZE

# Starting sequence
UART_PACKET_SEQUENCE = 0

##########
# API    #
##########

API_URL = "http://localhost:3000/sensors"

def send_data_to_api(sdata):
    dataArray = sdata.decode('utf-8').split(',')
    for ssensor in dataArray:
        sensor = ssensor.split(':')
        print("{\"value\":"+sensor[1]+"}")
        r = requests.patch(API_URL+'/'+sensor[0], json={"value": int(sensor[1])})
        print(r.json())

    #print(dataArray)



ser0 = initUART(SERIALPORT0)

openSerial(ser0)

try:
    while ser0.isOpen() : 
        data = uart_read(ser0)
        if data:
            print(data)
            send_data_to_api(data)





except (KeyboardInterrupt, SystemExit):
    closeUART(ser0)
    #closeUART(ser1)
    exit()